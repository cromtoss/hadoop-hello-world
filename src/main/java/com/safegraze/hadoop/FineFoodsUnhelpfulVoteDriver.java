package com.safegraze.hadoop;


import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * Driver program for the Hadoop Map/Reduce task to analyze reviews for unhelpful votes.
 *
 * @author Tom Cross
 */
public class FineFoodsUnhelpfulVoteDriver {

    public static void main(String[] args) throws ClassNotFoundException, IOException, InterruptedException {
        Job job = Job.getInstance();
        job.setJarByClass(FineFoodsUnhelpfulVoteDriver.class);
        job.setJobName("countUnhelpfulFineFoodsReviewers");

        TextInputFormat.addInputPath(job, new Path("/home/cromtoss/fine-foods/Reviews.csv"));

        FileOutputFormat.setOutputPath(job, new Path("/home/cromtoss/reviewers"));
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setMapperClass(FineFoodsUnhelpfulVoteMapper.class);
        job.setCombinerClass(FineFoodsUnhelpfulVoteReducer.class);
        job.setReducerClass(FineFoodsUnhelpfulVoteReducer.class);

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
