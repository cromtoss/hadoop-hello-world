package com.safegraze.hadoop;

import com.google.common.collect.Lists;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.List;

/**
 * Reducer for counting unhelpful review votes from the Amazon Fine Foods reviews dataset.
 *
 * @author Tom Cross
 */
public class FineFoodsUnhelpfulVoteReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

    /**
     * Count the total number of unhelpful votes received for each key (user).
     */
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        List<IntWritable> unhelpfulVotesByReview = Lists.newArrayList(values);
        int totalUnhelpfulVotes = unhelpfulVotesByReview
                .stream()
                .mapToInt(IntWritable::get)
                .sum();

        context.write(key, new IntWritable(totalUnhelpfulVotes));
    }
}
