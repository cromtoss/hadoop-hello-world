package com.safegraze.hadoop;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

/**
 *  Mapper for counting unhelpful review votes from the Amazon Fine Foods reviews dataset.
 *
 *  Note: The format for this dataset is:
 *      Id,ProductId,UserId,ProfileName,HelpfulnessNumerator,HelpfulnessDenominator,Score,Time,Summary,Text
 *
 * @author Tom Cross
 */
public class FineFoodsUnhelpfulVoteMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    // Ensures that commas between double quotes are not counted.
    // E.g., split("a, "b, c, d", e") should return a collection of size 3: ["a", "b, c, d", "e"]
    private static final Pattern SPLIT_COMMAS_IGNORE_NESTED = Pattern.compile(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");


    // Emit the number of unhelpful votes for this review.
    // Key is position in the file (not used).
    // Value is the full record entry for a single review.

    /**
     * Emit the number of unhelpful votes for a provided review.
     * Assumes the key is any arbitrary {@link LongWritable}.
     * Assumes the value is the full text of the record.
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String review = value.toString();
        List<String> elements = Lists.newArrayList(
                Splitter.on(SPLIT_COMMAS_IGNORE_NESTED).trimResults().split(review)
        );

        // Map elements to values we need to operate on.
        String user = elements.get(2);
        Integer helpfulVotes = Integer.parseInt(elements.get(4));
        Integer totalVotes = Integer.parseInt(elements.get(5));

        // If there are no unhelpful votes for this review, do nothing.
        Integer unhelpfulVotes = (totalVotes - helpfulVotes);
        if (unhelpfulVotes > 0) {
            context.write(new Text(user), new IntWritable(unhelpfulVotes));
        }
    }
}
