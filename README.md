Hadoop Hello World
==================

This project has two purposes:

_First_, this project provides a working Maven POM file for simple Hadoop projects based in Hadoop 2.7.

_Second_, this provides a working toy application to demonstrate a simple Map/Reduce application.
This application uses data provided from Kaggle.com under a Creative Commons open source license: see https://www.kaggle.com/snap/amazon-fine-food-reviews
